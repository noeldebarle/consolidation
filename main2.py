import requests
import json


def isVegan(requestAsJson):
    ingredients = requestAsJson["product"]["ingredients"]
    print(ingredients)
    return True


def requeteSurNumeroProduit(numero):
    x = requests.get(
        'https://world.openfoodfacts.org/api/v0/product/' + str(numero))
    return x


def ecrireJsonDansFichier(monJson, nomFichier):
    fichier = open(nomFichier, "a")
    fichier.write(json.dumps(monJson))
    fichier.close()


ecrireJsonDansFichier(
    requeteSurNumeroProduit(
        3468570116601).json(), "json_load/rozana.json")
ecrireJsonDansFichier(
    requeteSurNumeroProduit(
        3256540001305).json(), "json_load/brioche.json")
