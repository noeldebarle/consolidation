# consolidation

Conception de logiciel. TP5 : consolidation.

## utilisation

Le fichier main.py est une ébauche de serveur. 
Il se contente de lancer le serveur fastAPI d'exemple.

Le fichier main2.py permet de tester les requêtes.
Il lance une requête sur les produits rozana et brioche.
Il stocke ensuite les résultats dans le repertoire data_load pour pouvoir disposer de deux exemples.

Merci d'être passé sur mon dépot. See you!

